import entity.Sender;
import terminalServer.NotEnoughMoneyException;

public class TransferToMyCard extends TerminalImpl{

    public TransferToMyCard(Sender sender) {
        super(sender);
    }

    @Override
    protected void transfer(int amount) throws NotEnoughMoneyException {
        server.doTransferToCard(sender, amount);
    }
}
