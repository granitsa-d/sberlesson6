import frodMonitor.FrodMonitor;
import entity.Sender;
import frodMonitor.WrongAmountException;
import pin.PinValidator;
import pin.WrongPinException;
import terminalServer.NotEnoughMoneyException;
import terminalServer.TerminalServer;

public abstract class TerminalImpl implements Terminal{
    protected Sender sender;
    protected final TerminalServer server;
    protected final PinValidator pinValidator;
    protected final FrodMonitor frodMonitor;

    public TerminalImpl(Sender sender) {
        this.pinValidator = new PinValidator(sender);
        this.server = new TerminalServer();
        this.frodMonitor = new FrodMonitor();
        this.sender = sender;
    }

    @Override
    public void enterPin(int pin){
        try {
            pinValidator.checkPin(pin);
        } catch (WrongPinException wrongPin) {
            System.out.println(wrongPin);
        }
    }

    @Override
    public void doTransfer(int amount){
        try {
            pinValidator.checkPin();
            frodMonitor.setAmount(amount);
            frodMonitor.checkAmount();
            transfer(amount);
            System.out.println("Операция прошла успешно. Сумма перевода: " + amount);
        } catch (WrongAmountException | WrongPinException | NotEnoughMoneyException wrongAmount) {
            System.out.println(wrongAmount);
        }

    }

    protected abstract void transfer(int amount) throws NotEnoughMoneyException;
}

