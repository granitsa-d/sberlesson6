package pin;

import entity.Sender;

import java.util.Date;

public class PinValidator {
    private boolean canUse = false;
    int countTry;
    private Date date = new Date();
    private Sender sender;

    public PinValidator(Sender sender) {
        this.sender = sender;
    }


    public void checkPin(int pin) throws WrongPinException {
        if ((new Date()).getTime() < date.getTime()) {
            countTry = 0;
            throw new WrongPinException("Подождите 5 секунд");
        }

        if (sender.getPin() == pin) {
            countTry = 0;
            canUse = true;
        } else {
            countTry++;
            if (countTry == 3) {
                date = new Date();
                date.setSeconds(new Date().getSeconds() + 5);
            }
            throw new WrongPinException("Неверныый пин");

        }
    }

    public void checkPin() throws WrongPinException {
        if (canUse != true)
            throw new WrongPinException("Введите пин");
    }




}
