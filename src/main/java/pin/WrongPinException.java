package pin;

public class WrongPinException extends Exception{
    String massage;

    public WrongPinException(String message) {
        this.massage = message;
    }

    @Override
    public String toString() {
        return massage;
    }
}
