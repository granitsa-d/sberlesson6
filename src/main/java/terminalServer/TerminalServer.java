package terminalServer;

import entity.Person;
import entity.Sender;

import java.math.BigDecimal;

public class TerminalServer {

    public void doTransferToCard(Person person, int amount) throws NotEnoughMoneyException {
        checkAccBalance(person, amount);
        person.setBalanceAccount(person.getBalanceAccount().subtract(new BigDecimal(amount)));
        person.setBalanceCard(person.getBalanceCard().add(new BigDecimal(amount)));

    }

    public void doTransferToCard(Person sender, Person recipient, int amount) throws NotEnoughMoneyException {
        checkCardBalance(sender, amount);
        sender.setBalanceCard(sender.getBalanceCard().subtract(new BigDecimal(amount)));
        recipient.setBalanceCard(recipient.getBalanceCard().add(new BigDecimal(amount)));

    }

    public void doTransferToAcc(Sender person, int amount) throws NotEnoughMoneyException {
        checkCardBalance(person, amount);
        person.setBalanceCard(person.getBalanceCard().subtract(new BigDecimal(amount)));
        person.setBalanceAccount(person.getBalanceAccount().add(new BigDecimal(amount)));
    }



    private void checkCardBalance(Person person, int amount) throws NotEnoughMoneyException {
        if (!person.isEnoughMoneyCard(amount)) {
            throw new NotEnoughMoneyException("Не достаточно средств на карте");
        }
    }

    private void checkAccBalance(Person person, int amount) throws NotEnoughMoneyException {
        if (!person.isEnoughMoneyAcc(amount)) {
            throw new NotEnoughMoneyException("Не достаточно средств на счету");
        }
    }

    public void doTransfer(int amount) {

    }
}
