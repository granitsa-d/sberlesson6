package terminalServer;

public class NotEnoughMoneyException extends Exception{
    String massage;

    public NotEnoughMoneyException(String message) {
        this.massage = message;
    }

    @Override
    public String toString() {
        return massage;
    }
}
