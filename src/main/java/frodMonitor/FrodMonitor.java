package frodMonitor;

public class FrodMonitor {
    private int amount;

    public FrodMonitor() { }

    public FrodMonitor(int amount) {
        this.amount = amount;
    }


    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void checkAmount() throws WrongAmountException {
        checkMultiplicity();
        checkMoreThan(1000);
    }

    private void checkMultiplicity() throws WrongAmountException {
        if (amount % 100 != 0)
            throw new WrongAmountException("Некорректная сумма.");
    }

    private void checkMoreThan(int maxAmount) throws WrongAmountException {
        if (amount > maxAmount)
            throw new WrongAmountException("Сумма не должна превышать " + maxAmount);
    }
}
