package frodMonitor;

public class WrongAmountException extends Exception{
    String massage;

    public WrongAmountException(String message) {
        this.massage = message;
    }

    @Override
    public String toString() {
        return massage;
    }
}
